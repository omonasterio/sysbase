@section('css')
    @include('layouts.datatables_css')
@endsection
@extends('layouts.app')

@section('htmlheader_title')
    Pregunta 
@endsection

@include('layouts.plugins.jquery-ui')

@push('css')
    <!-- Ionicons -->
    <!-- <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css"> -->
    <!-- Theme style -->
    {{--<link rel="stylesheet" href="dist/css/adminlte.min.css">--}}
    <!-- iCheck -->
    <link rel="stylesheet" href="{{asset('plugins/iCheck/flat/blue.css')}}">
    <!-- Morris chart -->
    <link rel="stylesheet" href="{{asset('plugins/morris/morris.css')}}">
    <!-- jvectormap -->
    <link rel="stylesheet" href="{{asset('plugins/jvectormap/jquery-jvectormap-1.2.2.css')}}">
    <!-- Date Picker -->
    <link rel="stylesheet" href="{{asset('plugins/datepicker/datepicker3.css')}}">
    <!-- Daterange picker -->
    <link rel="stylesheet" href="{{asset('plugins/daterangepicker/daterangepicker-bs3.css')}}">


    @endpush

@section('content')
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col">
                    <h1 class="m-0 text-dark">Dashboard</h1>
                </div>
                <!-- 
                    head de la pag
                 -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">        
                @include('flash::message')
                @include('adminlte-templates::common.errors')
            <div class="row">
                <!-- formulario de la consulta  -->

                <div class="card">
                    <div class="card-body">
                            <form method="POST" action="{{ route('pregunta.store') }}"  role="form">
                                    {{ csrf_field() }}
                            
                                <div class="form-row">

                                    <!-- ('titulo');
                                    ('descripcion'); -->

                                <!-- Username Field -->
                                <div class="form-group col-sm-10">
                                    <label for="pregunta">Pregunta:</label>
                                    <input class="form-control" name="pregunta" placeholder="pregunta de la consulata" type="text" id="pregunta">
                                </div>

                                <!-- Name Field -->
                                <div class="form-group col-sm-10">
                                    <label for="descripcion">descripcion:</label>
                                    <input class="form-control" name="descripcion" placeholder="descripcion de la consulata" type="text" id="descripcion">
                                </div>

                                <!-- Email Field -->
                                <!-- <div class="form-group col-sm-6">
                                    <label for="email">Email:</label>
                                    <input class="form-control" name="email" type="email" id="email">
                                </div> -->

                             

                                    <!-- Submit Field -->
                                    <div class="form-group col-sm-12">

                                        <button type="submit" onclick="this.form.submit(); this.disabled=true;"
                                            class="btn btn-outline-success waves-effect waves-light">Guardar</button>
                                        <a href="{{ route('dashboard') }}"
                                        class="btn btn-outline-default waves-effect waves-light">Cancelar</a>
                                    </div>
                            </div>
                        </form>
                    </div>
                </div>
                
                <!-- formulario de la consulta  -->
        
            </div>

            <div class="table-responsive">
                {!! $dataTable->table(['width' => '100%']) !!}
            </div>


        </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
@endsection

@section('scripts')
@include('layouts.datatables_js')
{!! $dataTable->scripts() !!}
<script>
    $(function () {
            var dt = window.LaravelDataTables["dataTableBuilder"];
            
            //Cuando dibuja la tabla
            dt.on( 'draw.dt', function () {
                $(this).addClass('table-sm table-striped table-bordered table-hover');
                $('[data-toggle="tooltip"]').tooltip();
            });
            
        })
        </script>
@endsection
@push('scripts')
    <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
    <!-- <script>
        $.widget.bridge('uibutton', $.ui.button)
    </script> -->

    <!-- Morris.js charts -->
    <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script> -->
    <script src="{{asset('plugins/morris/morris.min.js')}}"></script>
    <!-- Sparkline -->
    <script src="{{asset('plugins/sparkline/jquery.sparkline.min.js')}}"></script>
    <!-- jvectormap -->
    <script src="{{asset('plugins/jvectormap/jquery-jvectormap-1.2.2.min.js')}}"></script>
    <script src="{{asset('plugins/jvectormap/jquery-jvectormap-world-mill-en.js')}}"></script>
    <!-- jQuery Knob Chart -->
    <script src="{{asset('plugins/knob/jquery.knob.js')}}"></script>

    <!-- daterangepicker -->
    <script src="{{asset('js/moment/moment.min.js')}}"></script>
    <script src="{{asset('js/moment/es.js')}}"></script>
    <script src="{{asset('plugins/daterangepicker/daterangepicker.js')}}"></script>

    <!-- datepicker -->
    <script src="{{asset('plugins/datepicker/bootstrap-datepicker.js')}}"></script>

    <!-- Bootstrap WYSIHTML5 -->
    {{--<script src="plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>--}}
    <!-- Slimscroll -->
    <script src="{{asset('plugins/slimScroll/jquery.slimscroll.min.js')}}"></script>
    <!-- FastClick -->
    <script src="{{asset('plugins/fastclick/fastclick.js')}}"></script>

    <!-- AdminLTE dashboard demo (This is only for demo purposes) -->
    <script src="{{asset('js/dashboard.js')}}"></script>
    <!-- AdminLTE for demo purposes -->
    {{--<script src="dist/js/demo.js"></script>--}}
@endpush
