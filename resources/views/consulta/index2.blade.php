@section('css')
@include('layouts.datatables_css')
@endsection

@extends('layouts.app')

@section('htmlheader_title')
Constas reponder
@endsection


@section('content')
<!-- Content Header (Page header) -->
<div class="content-header">
  <div class="container-fluid">

    @include('flash::message')
    @include('adminlte-templates::common.errors')


    <div class="row mb-2">
      <div class="col">
        <h1 class="m-0 text-dark"> Responder la Conslata </h1>
      </div><!-- /.col -->

     

    </div><!-- /.row -->
  </div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->

<!-- Main content -->
<section class="content">

  <!-- Small boxes (Stat box) -->
  <div class="col-md-12 mb-8">

     <!-- Main content -->
     <section class="content">
        <div class="container-fluid">
          @include('flash::message')
          @include('adminlte-templates::common.errors')
          <div class="row">


            <div class="table-responsive">
              {!! $dataTable->table(['width' => '100%']) !!}
            </div>

          </div>

        </div><!-- /.container-fluid -->
      </section>
      <!-- /.content -->


  </div>

  </div><!-- /.container-fluid -->
</section>
<!-- /.content -->
@endsection

@section('scripts')
@include('layouts.datatables_js')
{!! $dataTable->scripts() !!}
<script>
  $(function () {
    var dt = window.LaravelDataTables["dataTableBuilder"];

    //Cuando dibuja la tabla
    dt.on('draw.dt', function () {
      $(this).addClass('table-sm table-striped table-bordered table-hover');
      $('[data-toggle="tooltip"]').tooltip();
    });

  })
</script>
@endsection