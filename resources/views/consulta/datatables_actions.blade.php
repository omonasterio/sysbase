


<span data-toggle="tooltip" title="Ver">
    <button class='btn btn-default btn-xs' data-toggle="modal" data-target="#modaShowPermiso{{$id}}">
        <i class="fa fa-eye"></i>
    </button>
</span>



<!-- Modal-->
<div class="modal fade" id="modal-delete-{{$id}}" tabindex="-1" role="dialog" aria-labelledby="modalLogoutLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content ">
            <div class="modal-header">
                <h5 class="modal-title" >
                    <i class="fa fa-warning text-warning fa-2x" aria-hidden="true"></i> &nbsp;¿Eliminar?
                </h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">Seleccione "SI" a continuación si está seguro de eliminar el registro.</div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
                {!! Form::open(['route' => ['permissions.destroy', $id], 'method' => 'delete']) !!}
                    <button type="submit" class="btn btn-danger">SI</button>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>
<!-- /.modal -->


<!-- Modal -->
<div class="modal fade" id="modaShowPermiso{{$id}}" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="modelTitleId">Permiso</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action = "{{route ('consultas.store', $id)}}" method = "GET" >  
                    @csrf
                    <input type="hidden" value="{{ $id }}" name="id">
                    <input type="hidden" value="{{ $pregunta }}" name="pregunta">
             
           

            <!-- Username Field -->
            <div class="form-group col-sm-10">
                <label for="pregunta">Pregunta:</label>
                <br>
                    <span> {{ $pregunta  }} </span>
                </div>
            
             <!-- Name Field -->
             <div class="form-group col-sm-10">
                <label for="respuesta"> Su Respuestas :</label>
                <input class="form-control" name="respuesta" placeholder="respuesta de la consulata" type="text" id="respuesta">
            </div>

                    
             <!-- Submit Field -->
             <div class="form-group col-sm-12">

            <button type="submit" onclick="this.form.submit(); this.disabled=true;"
                class="btn btn-outline-success waves-effect waves-light">Guardar</button>

                </form>
               
                
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>
