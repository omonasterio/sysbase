@extends('layouts.app')

@section('htmlheader_title')
Constas Cerrdas
@endsection


@section('content')
<!-- Content Header (Page header) -->
<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col">
        <h1 class="m-0 text-dark"> Constas Cerrdas </h1>
      </div><!-- /.col -->

    </div><!-- /.row -->
  </div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->

<!-- Main content -->
<section class="content">

  <!-- Small boxes (Stat box) -->


  @foreach ($pregunta as $key => $pre)
  <!-- Card -->
  <div class="card" style="max-width: 44rem;">
    <!-- Card image -->
    <!-- foto  -->
    <!-- Card content -->
    <div class="card-body">

      <!-- Title -->
      <h4 class="card-title"><a> {{ ++$key  }}) {{ $pre['pregunta'] }} </a></h4>
      <!-- Text -->
      <p class="card-text"> fecha de la consulta - {{ $pre['created_at'] }} </p>

      <span> {{ $pre['descripcion'] }} </span>



    </div>

  </div>
  <!-- Card -->

  @endforeach


  </div>

  </div><!-- /.container-fluid -->
</section>
<!-- /.content -->
@endsection