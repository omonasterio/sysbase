@section('css')
    @include('layouts.datatables_css')
@endsection
@extends('layouts.app')

@section('htmlheader_title')
    Pregunta 
@endsection

@include('layouts.plugins.jquery-ui')


@section('content')
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col">
                    <h1 class="m-0 text-dark">Dashboard</h1>
                </div>
                <!-- 
                    head de la pag
                 -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">        
                @include('flash::message')
                @include('adminlte-templates::common.errors')
            <div class="row">
                <!-- formulario de la consulta  -->

                <div class="card">
                    <div class="card-body">
                            
                            
                                <div class="form-row">

                                    <!-- ('titulo');
                                    ('descripcion'); -->

                                <!-- Username Field -->
                                <div class="form-group col-sm-10">
                                    <label for="pregunta">Pregunta:</label>
                                    <br>
                                        <span> {{ $pregunta  }} </span>
                                    
                                </div>
                                {!! Form::open(['route' => 'consultas.store']) !!}
                               
                                        {{ csrf_field() }}
                           
                                <!-- Name Field -->
                                <div class="form-group col-sm-10">
                                    <label for="descripcion"> Su Respuestas :</label>
                                    <input class="form-control" name="descripcion" placeholder="descripcion de la consulata" type="text" id="descripcion">
                                </div>

                                                          

                                    <!-- Submit Field -->
                                    <div class="form-group col-sm-12">

                                        <button type="submit" onclick="this.form.submit(); this.disabled=true;"
                                            class="btn btn-outline-success waves-effect waves-light">Guardar</button>
                                        <a href="{{ route('dashboard') }}"
                                        class="btn btn-outline-default waves-effect waves-light">Cancelar</a>
                                    </div>
                                    {!! Form::close() !!}
                        </div>
                    </div>
                </div>
                
                <!-- formulario de la consulta  -->
        
            </div>

           

        </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
@endsection


