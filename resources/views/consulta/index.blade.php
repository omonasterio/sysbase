@section('css')
@include('layouts.datatables_css')
@endsection

@extends('layouts.app')

@section('htmlheader_title')
Constas reponder
@endsection


@section('content')
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row">
                <div class="col">
                    <h1 class="m-0 text-dark">
                        Responder la Conslata
                    </h1>
                </div><!-- /.col -->
                <div class="col ">
                    
                    
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
        <div class="container-fluid">
            @include('flash::message') 
            @include('adminlte-templates::common.errors')

            <div class="row">
                @foreach($pregunta as $pre)
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-body">
                       
                       
                            <div class="form-group col-sm-12" id="{{ $pre['id'] }}">
                                <label for="username" class=""> {{ $pre['pregunta'] }} </label>
                                <br>
                                <span>
                                      {{ $pre['descripcion'] }} 
                                </span>
                                <hr>
                         

                            <form method="POST" action="/consulta/respuestas/{{ $pre['id'] }}">
                                <input name="_token" type="hidden" value="{{ csrf_token() }}"/>
                                <input type="hidden" value="{{ $pre['id'] }}" name="id">
                                    <input class="form-control"  value="{{ $pre['pregunta'] }}" name="pregunta" type="hidden" id="pregunta">
                                    <input class="form-control" name="respuesta" type="text" id="respuesta">
                                    
                                
                                </div>
                                    
                                        <!-- Submit Field consultas.store -->
                                        <div class="form-group col-sm-12">
                                           
                                            <button type="submit" class="btn btn-outline-success">Guardar</button>
                                           
                                        </div>
                                    </form>
                                    
                                </div>
                    </div>
                    </div>
                    <!-- /.card -->
                </div>
                <!-- /.col-md-6 -->
                @endforeach
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </div>
    <!-- /.content -->


@endsection
