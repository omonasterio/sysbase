@extends('layouts.app')

@section('htmlheader_title')
	ver los resultado de la consulta 
@endsection

@section('content')
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-12">
                    <h1 class="m-0 text-dark"> los resultado de la consulta  </h1>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->


    <!-- Main content -->
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">


                   
                    <div class="card">

                        <div class="card-body">
                          
                     
                                <!-- Card -->
                                <div class="card">
                                    <!-- Card image -->
                                    <!-- foto  -->
                                    <!-- Card content -->
                                    <div class="card-body">

                                    <!-- Title -->
                                    <h4 class="card-title" style=" text-transform: uppercase;"><a> {{ $pregunta }} </a></h4>
                                        <hr>
                                    <!-- Text -->
                                    @foreach ($ver as $key => $pre)
                                    <p class="card-text"> fecha de la consulta - {{ $pre['fecha'] }} </p>
                                    <span> {{ $pre['respuesta'] }} </span>
                                    <br>
                                    <hr>

                                    @endforeach


                                    </div>

                                </div>
                                <!-- Card -->

                     

                        </div>
                    </div>
                    <!-- /.card -->
                </div>
                <!-- /.col-md-6 -->
            </div>
            <!-- /.row -->
           
                

            <div class="row">
                <div class="form-group col-sm-12">
                <a href="{!! route('pregunta.index') !!}" class="btn btn-default">Regresar</a>
                </div>
            </div>
        </div>
        <!-- /.container-fluid -->
    </div>
    <!-- /.content -->
@endsection
