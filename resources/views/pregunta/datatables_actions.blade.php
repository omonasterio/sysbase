
<a href="{{ route('pregunta.show', $id) }}" class='btn btn-default btn-xs' data-toggle="tooltip" title="Ver">
    <i class="fa fa-eye"></i>
</a>

<a href="{{ route('pregunta.edit', $id) }}" class='btn btn-info btn-xs' data-toggle="tooltip" title="Editar">
    <i class="fa fa-edit"></i>
</a>

<!--
-->
<a href="{{ route('preguntas.eliminar', $id) }}" class="btn btn-danger btn-xs">
    <span class="fa fa-trash-alt" data-toggle="tooltip" title="Eliminar{{$id}}"></span>
</a> 

