<!-- Pregunta Field -->
<div class="form-group col-sm-10">
    {!! Form::label('pregunta', 'lap pregunta de la consulta:') !!}
    {!! Form::text('pregunta', null, ['class' => 'form-control']) !!}
</div>

<!-- descripcion Field -->
<div class="form-group col-sm-10">
    {!! Form::label('descripcion', 'Descripcion:') !!}
    {!! Form::text('descripcion', null, ['class' => 'form-control']) !!}
</div>


<div class="form-group col-sm-12" style="padding: 0px; margin: 0px">
</div>

@if(!isset($create))
<div class="form-group col-sm-12">
    <a class="" role="button" data-toggle="collapse" href="#collapseExample" aria-expanded="false"
        aria-controls="collapseExample">
        <span class=" btn btn-outline-primary">Editar también contraseña</span>
    </a>
</div>
@endif


<div class="col-sm-12 {{ isset($create) ? "collapse" : '' }}" id="collapseExample">
    <div class="form-row">

        <!-- Password Field -->
        <div class="form-group col-sm-6">
               
                
                        <label for="status"> status </label>
                        <select class="form-control" value="" name="status" id="status">
                          <option value="0"> abierto </option>
                          <option value="1"> cerrado </option>
                         
                        </select>
                      </div>
                
        </div>

        
    </div>
</div>