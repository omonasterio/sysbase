<?php

Route::get('pruebas', function () {
 
});
// vista comun
Route::get('/home', 'HomeController@index')->name('home');
Route::get('/', 'DashboardController@index');
Route::get('/dashboard', 'DashboardController@index')->name('dashboard');
Route::resource('pregunta', 'PreguntaController')->except([
    'destroy'
]);

Route::get('preguntas/{id}', 'PreguntaController@eliminar')->name('preguntas.eliminar');

// Route::delete('/preguntas/{id}', 'PreguntaController@eliminar')
//     ->where('id', '[0-9]+')
//     ->name('preguntas.eliminar');


Route::get('user/profile/{user}', 'UserController@editProfile')->name('user.edit.profile');;
Route::patch('user/profile/{user}', 'UserController@updateProfile')->name('user.update.profile');;

Route::resource('configurations', 'ConfigurationController');
Route::resource('rols', 'RolController');
Route::resource('users', 'UserController');

Route::get('user/{user}/menu', 'UserController@menu')->name('user.menu');;
Route::patch('user/menu/{user}', 'UserController@menuStore')->name('users.menuStore');


Route::get('option/create/{padre}', 'OptionMenuController@create')->name('option.create');
Route::get('option/orden', 'OptionMenuController@updateOrden')->name('option.order');
Route::get('option/orden', 'OptionMenuController@updateOrden')->name('option.order.store');
Route::resource('options',"OptionMenuController");

Route::get('oauth/client',"UserController@adminOauthClient")->name('oauth.clients');


Route::get('prueba/pdf', function (\App\Extensiones\Fpdf $fpdf) {
    $fpdf->AddPage();
    $fpdf->SetFont('Courier', 'B', 18);
    $fpdf->Cell(50, 25, 'Hello World!');
    $fpdf->Output();
    exit();
})->name('prueba.pdf');

Route::resource('permissions', 'PermissionController');

Route::get('/consulta/respuesta',        'ConsultaController@index')->name('consulta.index');
Route::get('/consulta/respuesta/{id}', 'ConsultaController@stor')->name('consulta.stor');
Route::post('/consulta/respuestas/{id}',  'ConsultaController@store')->name('consultas.store');

Route::get('/consulta/respondidas',     'ConsultaController@edit')->name('consulta.edit');
Route::get('/consulta/cerradas',        'ConsultaController@show')->name('consulta.show');

//consulta/respuesta/{$id}
 

/*

Route::get('/customers', 'CustomerController@index')
->name('customerIndex');
Route::get('/customers/create', 'CustomerController@create')
->name('customerCreate');
Route::post('/customers', 'CustomerController@store')
->name('customerStore');
Route::get('/customers/{customer}', 'CustomerController@show')
->name('customerShow');
Route::get('/customers/{customer}/edit', 'CustomerController@edit')
->name('customerEdit');
Route::put('/customers/{customer}', 'CustomerController@update')
->name('customerUpdate');
Route::delete('/customers/{customer}', 'CustomerController@destroy')
->name('customerDestroy');
*/