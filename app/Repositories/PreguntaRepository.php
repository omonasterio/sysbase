<?php

namespace App\Repositories;

use App\Notas;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class UserRepository
 * @package App\Repositories
 * @version October 26, 2018, 6:59 pm CST
 *
 * @method User findWithoutFail($id, $columns = ['*'])
 * @method User find($id, $columns = ['*'])
 * @method User first($columns = ['*'])
*/
class PreguntaRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'pregunta',
        'descripcion',
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Notas::class;
    }
}
