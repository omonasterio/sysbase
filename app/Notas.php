<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Notas extends Model
{
    //
     protected $fillable = [
         'id_user',
         'pregunta',
         'descripcion',
         'status'

        ];

        
 
public function respuesta()
{   
    // notas.id
    // respuesta.id_users
    return $this->belongsTo('App\Respuesta', 'id_notas');
}

}

//pregunta 
// status
// id_user
// 
//
//
