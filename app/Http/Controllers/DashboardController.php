<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth; 
use App\Notas;
use DB;
use Flash;
use App\Respuesta;
use Illuminate\Http\Request;

class DashboardController extends Controller
{

    /**
     * DashboardController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        //$rols = Role::all();
        //$rolsUser= [];
        //$cerrada = Notas::select('id','pregunta','descripcion','status')->where('status', 1)->get()->toArray();
        //Respuesta::select('*')->where('id_user', $id)->get(); //la id mia en la respuesta 
        //$count = Notas::select('*')->where('status', 0)->where('status', 0)->get()->count();
        $id =  Auth::user()->id;

        $pregunta = Notas::all()->count();
        $count = DB::table('respuesta')->select('*')->where('id_users', '=', $id )->get()->count(); 
        
        $contestada = DB::table('respuesta')->where('id_users', $id)->where('status', '!=', 0)->get()->count(); 
        $cerrada = Notas::select('id','pregunta','descripcion','status')->where('status', 1)->get()->count();
        
        $cou = ($count != 0 ) ? $count : $pregunta;
        
       
       $counto = ($cou);

        //pregunta - total de pregunta -
        //count - suma de pregunta por rsponder -
       //contestada - las q e respondido
       //cerrada - las preguntas cerradas 
        
        return view('dashboard',compact('pregunta','counto','contestada','cerrada'));
       
        //return view('home',compact('count'));
       // dd( $pregunta);
    
        //return view('dashboard'); 
    }

    public function form()
    {
        //dd("aki");
        //$rols = Role::all();
        //$rolsUser= [];
        $pregunta =1;
        return view('pregunta',compact('pregunta'));
    
        //return view('dashboard'); 
    }


    public function store (Request $request)
    {
        //dd("aki");
        //$rols = Role::all();
        //$rolsUser= [];
        $pregunta =1;
        return view('pregunta',compact('pregunta'));
    
        //return view('dashboard'); 
    }

    
}

// |          GET|HEAD  | pregunta                                | pregunta.index                    | App\Http\Controllers\DashboardController@index                            | web,auth                                     |
// |        | POST      | pregunta                                | pregunta.store                    | App\Http\Controllers\DashboardController@store                            | web,auth                                     |
// |        | GET|HEAD  | pregunta/create                         | pregunta.create                   | App\Http\Controllers\DashboardController@create                           | web,auth                                     |
// |        | DELETE    | pregunta/{preguntum}                    | pregunta.destroy                  | App\Http\Controllers\DashboardController@destroy                          | web,auth                                     |
// |        | GET|HEAD  | pregunta/{preguntum}                    | pregunta.show                     | App\Http\Controllers\DashboardController@show                             | web,auth                                     |
// |        | PUT|PATCH | pregunta/{preguntum}                    | pregunta.update                   | App\Http\Controllers\DashboardController@update                           | web,auth                                     |
// |        | GET|HEAD  | pregunta/{preguntum}/edit               | pregunta.edit                     | App\Http\Controllers\DashboardController@edit                             | web,auth                                     |
// |     