<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use App\DataTables\ConsultaDataTable;
use \Illuminate\Support\Arr;
use App\Notas;
use App\Respuesta;

use DB;
use Flash;
use App\User;
use Illuminate\Http\Request;
use Response;

class ConsultaController extends Controller
{
    private $preguntaRepository;
    //PreguntasRepository $preguntaRepo
    public function __construct()
    {
        $this->middleware('auth');
        //$this->preguntaRepository = $preguntaRepo;
    }

    /**
     * DashboardController constructor.
     */
    
    public function index()
    {  
        $id =  Auth::user()->id;
        //dd($id);
        //$pregunta2 =  Notas::select('id','pregunta','descripcion','status','created_at')->where('status', 0)->get();
        //$pregunta2 = DB::table('notas')->select('id','pregunta','descripcion','status','created_at')->where('status', 0)->get();

        // $pregunta2 =  DB::table('notas') 
        //     ->join('respuesta', function ($join) {
        //                 $id =  Auth::user()->id;  
        //                 $join->on('notas.id', '=', 'respuesta.id_notas')
        //                 ->where('notas.id', '=', 'respuesta.id_notas');  // ! trae el que falta = trae todo id    
        //                 ->where('respuesta.id_users', '!=', $id);  // ! trae el que falta = trae todo id    
                        //->where('notas.id', '!=', $id);  // ! trae el que falta = trae todo id    
                        //->Where('notas.status', '!=', 0)
                        //->orderBy('respuesta.id_users', 'desc');
                    // })
                    // ->get();

                // $pregunta2 = DB::table('notas') 
                //     ->join('respuesta', function ($join) {
                //         $idx =  Auth::user()->id;    
                //         $join->on('notas.id', '=', 'respuesta.id_notas')
                //         ->where('notas.id', '=', 'respuesta.id_notas')
                //         ->where('notas.pregunta', '=', 'respuesta.pregunta')
                //         ->where('notas.status', '=', 0) //0
                     
                //         ->Where('respuesta.status', '=', '')
                //        // ->where('respuesta.status', '=', $idx)
                //        ->Where('respuesta.id_users', '!=', $idx)
                //         ->orderBy('respuesta.id', 'desc');
                // })
                // ->get();

                // DB::table("users")->select('*')
                // ->whereNOTIn('id',function($query){
                //    $query->select('user_id')->from('invite_users');
                // })
                // ->get();
                $id =  Auth::user()->id;   
                // $pregunta2 = DB::table("notas")->select('*')
                // ->where('status', '=', 0) //0
                // ->whereNOTIn('id_users',function($query){
                //    $query->select('id_users')->from('respuesta');
                //    //->whereNOTIn('id_users', '=', '1');
                // })
                // ->get();

                // esta fino menos no wherenotin id de la otra tabla
                // $pregunta2 = DB::table('notas')                 
                // ->select('id_user','pregunta','descripcion','status')
                // ->where('status', '=', 0) //0
                // ->whereNOTIn('notas.id', DB::table('respuesta')
                //     ->select('id_users')
                //     ->where('id_users', '=', $id)
                //     ->get()->toArray())
                // ->get();

                // $pregunta2 = DB::table('report_list')
                // ->join('users', 'report_list.user_id', '=', 'users.id')
                // ->where('report_list.site_name', '=', $site_name);
                // ->select('users.*', 'report_list.email_date','report_list.url','report_list.recipient')
                // ->get();

               // ->Where('respuesta.id_users', '!=', $idx)
                //         ->orderBy('respuesta.id', 'desc');

                // $pregunta2 = DB::table('notas') 
                //     ->join('respuesta', function ($join) {
                //             $id =  Auth::user()->id;  
                //             $join->on('notas.id', '=', 'respuesta.id_notas')
                //             ->Where('notas.status', '=', 0)
                //             ->where('notas.id', '!=', 'respuesta.id_notas')  // ! trae el que falta = trae todo id    
                //             ->where('respuesta.id_users', '=', $id)  // ! trae el que falta = trae todo id    
                //             ->where('notas.id', '=', $id);  // ! trae el que falta = trae todo id    
                //             //->orderBy('respuesta.id_users', 'desc')
                //         })
                //         ->get();

                $pregunta2 =DB::table('notas'
                )->select('notas.id', 'notas.pregunta','notas.descripcion','notas.status','notas.created_at')
                ->join('respuesta', function ($join) {
                    $idx =  Auth::user()->id;    
                    $join->on('notas.id', '=', 'respuesta.id_notas')
                    ->where('notas.status', '=', 0)
                    
                            ->Where('respuesta.id_users', '=', $idx)
                            
                            ->orderBy('notas.id', 'desc');
                    })
                    ->get();

                // esta fino menos no wherenotin id de la respuesta.idnotas 
                $pregunta1 = DB::table('notas')                 
                ->select('id_user','pregunta','descripcion','status')
                ->where('status', '=', 0) //0
                ->get();

               // $users = Notas::all();
                $blocks = [23,24,25];

                $users = Notas::all();
                $data = $users->reject(function ($user) use ($blocks) {
                    $uss = in_array($user->id, $blocks);
                });
              dd($data[2]['id']);
            
         
                

        dd($pregunta1);

        $pregunta= [];
        
        //dd($pregunta);
        foreach ($pregunta2 as $n) {
            $pregunta[]= [
                'id' => $n->id,
                'pregunta' => $n->pregunta,
                'descripcion' => $n->descripcion,
             'status' => $n->status,
             'created_at' => $n->created_at,
                
            ]; 
            
         }
         //dd($pregunta[0]['id']);
       //  dd($pregunta);
        return view('consulta.index',compact('pregunta'));
       //return $consultaDataTable->render('consulta.create');
    }

    public function index2()
    {
       // dd("consulta index ");
        //$rols = Role::all();
        // $cerrada = Notas::select('id','pregunta','descripcion','status')->where('status', 1)->get()->count();
        // $pregunta = Notas::all()->count();
       
        $id =  Auth::user()->id;
        //`pregunta`, `descripcion`, `status`,  
       $pregunta = Notas::select('id','pregunta','descripcion','status','created_at')->where('status', 0)->get()->toarray();
       
        $count = DB::table('respuesta')->select('*')->where('id_users', '=', $id )->where('status', '!=', $id )->get(); 
        $counto = DB::table('respuesta')->select('*')->where('id_users', '=', $id )->where('status', '=', $id )->get()->count(); 

        $contestada = DB::table('respuesta')->select('*')->where('id_users', $id)->where('status', '!=', $id)->get(); 
        
       // dd($counto);
       // $count = ($count != '' ) ?   $pregunta : $count;
       //$count = ($count != 0 ) ? $count : $pregunta;

       // $count = ($counto != 0)  ?  $count : $pregunta; // aki todo bien 
        
      //  dd($count); // si no se a respondido aki ok 
        
        if($counto != 0){
            $pregunta= [];
            $pregunta1= ($counto != 0)  ?  $count : $pregunta;

            //     foreach ($pregunta as $n) {
            //     $pregunta[]= [
            //         'id' => $n->id,
            //         'pregunta' => $n->pregunta,
                  
            //     ];
                
            // }

            /// fin del primer if

        } 
        
        if ($counto > 0){
            $pregunta2= [];
            $pregunta2 = ($counto > 0)  ?  $contestada : $pregunta; ;
            //dd($pregunta);
            foreach ($pregunta2 as $n) {
                $pregunta[]= [
                    'id' => $n->id,
                    'pregunta' => $n->pregunta,
                    'descripcion' => $n->respuesta,
                 'status' => $n->status,
                 'created_at' => $n->created_at,
                    
                ];
                
             }
         }

              /// fin del segunda  if

        
       // dd($pregunta);

        
        // $pre = DB::table('notas')->select('*')->where('status', 0)->get()->toarray();
        //$ut = [];

        // foreach ($pre as $n) {
        //     $pregunta[]= [
        //         'id' => $n->id,
        //         'pregunta' => $n->pregunta,
        //         'descripcion' => $n->descripcion,
        //         'status' => $n->status,
        //         'created_at' => $n->created_at,
        //     ];
             
        //    }
           //dd($ut);
            // ver   creo q esta bien 
            //     $pregunta =  DB::table('respuesta') 
            //        ->join('notas', function ($join) {
            //            $idx =  Auth::user()->id;    
            //            $join->on('notas.id', '=', 'respuesta.id_notas')
            //            ->where('notas.status', '=', 0)
            //            ->where('respuesta.status', '=', 0)
            //            ->Where('respuesta.id_users', '=', $idx)
            //            ->orderBy('respuesta.id', 'desc');
            //    })
            //    ->get();

            //mala
            // $preguntax =  DB::table('notas') 
            //         ->join('respuesta', function ($join) {
            //             $idx =  Auth::user()->id;    
            //             $join->on('notas.id', '=', 'respuesta.id_notas')
            //                     //->where('notas.status', '=', 0)
            //                     //->where('respuesta.status', '!=', 1)
            //                     ->where('respuesta.id_users', '=', $idx);
            //                 })
            //                 ->get();

            // dd($pregunta);
            // return view('pregunta',compact('pregunta'));
            return view('consulta.index',compact('pregunta'));
            //return $preguntaDataTable->render('pregunta.index');
    
        //return view('dashboard'); 
    }


    public function show()
    {   
       // dd("consulta show");
       $id = Auth::user()->id;
        //$pregunta = Notas::select('id','pregunta','descripcion','status')->where('status', 3)->get()->count();
       // $pregunta = DB::table('respuesta')->where('id_notas', $id)->get()->count();

        $pre =  DB::table('notas')->select('*')->where('status', 1)->get()->toarray();

         $pregunta = [];

         foreach ($pre as $n) {
            $pregunta[]= [
                'id' => $n->id,
                'pregunta' => $n->pregunta,
                'descripcion' => $n->descripcion,
                'status' => $n->status,
                'created_at' => $n->created_at,
            ];
             
           }
          

        //dd($pregunta);
        return view('consulta.mostrar',compact('pregunta'));
    
       // return view('consulta.index'); 
    }

    public function edit()
    {   
        $pre = [];
        $pro = [];

        $id = Auth::user()->id; 
        $pre = DB::table('respuesta')->select('*')->where('id_users', $id)->get()->toarray();
        $n   = DB::table('respuesta')->select('*')->where('id_users', $id)->get()->count();
        $pro = DB::table('notas')->select('*')->where('id', $id )->get()->toarray();
        
        //dd($n);
        if (empty($n)) {
            Flash::error('no hay consulta');

            return redirect(route('dashboard'));
        }
        
        $pregunta = [];
         foreach ($pre as $key => $n) {
            $pregunta[$key]= [
                'id' => $n->id,
                'id_notas' => $n->id_notas,
                'pregunta' => $n->pregunta,
                'respuesta' => $n->respuesta,
                'fecha' => $n->fecha,
                'created_at' => $n->created_at,
            ];

             
           }
           // cuando es ceso no cargar 

           // dd($pregunta[]['pregunta']);

             //dd($pre); 
            //dd($pro); 
            //dd($pregunta[0]['id_notas']);   
        return view('consulta.editar',compact('pregunta'));
    }

    public function stor($id)
    {   
        dd($id);
        //dd("segunda paso");
        $id = 19;
        $pregunta = "la pizza para la cena ";
      // Flash::success('Respuesta guardada exitosamente.');
      return view('consulta.form',compact('pregunta'));
       
   }
    
    public function store(Request $request, $id,  Respuesta $Respuesta )
    {   
       //dd($id);
       //dd($request->all());
        
        $request->validate([
            'id'=>'required',
            'pregunta'=>'required',
            'respuesta'=>'required',
        ]);
        $id = $request->id;
        $idx = Auth::user()->id;
        $v = Respuesta::select('*')->where('id_notas', $id)->where('id_users', $idx)->get()->count();  
        
       // dd($v);
       
        if (!empty($v)) {
            Flash::error('Consulta realizada');

            return redirect(route('consulta.index'));
        }
        // dd($v);
            
            $num = 1;
            $fecha = date("d-m-Y"); 
            $personas_id = Auth::user()->id;
            //$fecha2 = date("Y-m-d H:i:s"); 
      
            //  Notas::create($request->all());

            // $data = DB::table('respuesta')->insert( [
            //     'id_notas'     => $request->id, 
            //     'id_users'      => $personas_id,
            //     'pregunta'    => $request->pregunta,
            //     'respuesta'    => $request->respuesta,
            //     'fecha'        => $fecha,
            //     'status'        =>  $personas_id,
            //     'created_at' 	=> $fecha2,
            //     'updated_at'	=> $fecha2,
            // ]);
            //$data->save();

            $respuesta = new Respuesta([
                'id_notas'  => $request->id, 
                'id_users'  => $personas_id,
                'pregunta'  => $request->pregunta,
                'respuesta' => $request->respuesta,
                'fecha'     => $fecha,
                'status'    =>  $personas_id,
                //'created_at' 	=> $fecha2,
                //'updated_at'	=> $fecha2,
            ]);
            $respuesta->save();

            //  $flight = Respuesta::find($request->id);
            //  $flight->status = $personas_id;
            //  $flight->save();
         
        Flash::success('Respuesta guardada exitosamente.');

        return redirect(route('consulta.index'));
    }

}