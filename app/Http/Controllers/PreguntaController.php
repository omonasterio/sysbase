<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use App\DataTables\PreguntaDataTable;
use App\Repositories\PreguntasRepository;
use App\Notas;
use App\Respuesta;

use DB;
use Flash;
use App\User;
use Illuminate\Http\Request;
use Response;

class PreguntaController extends Controller
{
    private $preguntaRepository;
    //PreguntasRepository $preguntaRepo
    public function __construct()
    {
        $this->middleware('auth');
        //$this->preguntaRepository = $preguntaRepo;
    }

    /**
     * DashboardController constructor.
     */
    

    public function index(PreguntaDataTable $preguntaDataTable)
    {
        //$rols = Role::all();
        //$rolsUser= [];
        $pregunta =1;
       // dd( $pregunta);
       // return view('pregunta',compact('pregunta'));
       return $preguntaDataTable->render('pregunta.index');
    
        //return view('dashboard'); 
    }


    public function show( $id )
    {   
        //dd($id);
        $v = Respuesta::select('*')->where('id_notas', $id)->get()->count();  
        
        //dd($v);
        if (empty($v)) {
            Flash::error('Consulta no encontrado');

            return redirect(route('pregunta.index'));
        }
        
        
        $ve = [];
        $ve = Respuesta::select('*')->where('id_notas', $id)->get(); 
         $ver = Respuesta::select('*')->where('id_notas', $id)->get(); 
         $pregunta = ($ver[0]['pregunta']);
        
       


        $ver = [];
        foreach ($ve as $key => $n) {
            $ver[]= [
                'id' => $n->id,
               'respuesta' => $n->respuesta,
                'fecha' => $n->fecha,
                //'status' => $n->status,
            ];

             
           }

          // dd($ver);
           // c
        
        // id" => 1
        // "id_notas" => 18
        // "id_users" => 1
        // "pregunta" => "compra un usb"
        // "respuesta" => "si dale"
        // "fecha" => "11-05-2020"
        // "status" => 1
        // "created_at" => "2020-05-11 16:24:43"
        // "updated_at" => "2020-05-11 16:24:43"
        // "deleted_at" => null
        
        return view('pregunta.show',compact('ver', 'pregunta'));
        //return view('dashboard'); 
    }

    public function edit($id)
    {   
        //dd("aki estoy editar");
        //dd($id);
        $edit = Notas::find($id);
        //DB::table('notas')->select('*')->where('id', 1)->get()->toarray(); 
        //dd($edit->id);
        if (is_null($edit)) {
            Flash::error('Consulta no encontrado');

            return redirect(route('pregunta.index'));
        }


        return view('pregunta.editar',compact('edit'));
    }

    public function form()
    {
        //dd("aki");
        //$rols = Role::all();
        //$rolsUser= [];
        $pregunta =1;
        return view('pregunta',compact('pregunta'));
    
        //return view('dashboard'); 
    }

    public function create()
    {
        $rols = Notas::all();
        $rolsUser= [];
        $create =1;
        return view('pregunta.create',compact('rols','rolsUser','create'));
    }

    public function store (User $user, Request $request)
    {   
        DB::beginTransaction();
        $personas_id = Auth::user()->id;
       // dd($personas_id);
       // dd($request->all());
       // dd($request->descripcion);
        $request->validate([
            'pregunta'=>'required',
            'descripcion'=>'required',
            //'email'=>'required'
        ]);
        // 'id_user',
        // 'pregunta',
        // 'descripcion',
        // 'status'
       $num = 0;

      //  Notas::create($request->all());

      $nota = new Notas([
          'id_user'     => $personas_id,
          'pregunta'    => $request->pregunta,
          'descripcion' => $request->descripcion,
          'status'      => $num
      ]);
      $nota->save();

        //	$vehiculo = $id == 0 ? new Vehiculo() : Vehiculo::find($id);
        // $contact = new Contact([
        //     'first_name' => $request->get('first_name'),
        //     'last_name' => $request->get('last_name'),
        //     'email' => $request->get('email'),
        //     'job_title' => $request->get('job_title'),
        //     'city' => $request->get('city'),
        //     'country' => $request->get('country')
        // ]);
        // $contact->save();

        DB::commit();


        Flash::success('Pregunta guardado exitosamente.');

        return redirect(route('pregunta.index'));
        //return redirect('/dashboard')->with('success', 'Contact saved!');


        //return view('pregunta',compact('pregunta'));
        //return view('dashboard'); 
    }

    
    public function eliminar($id)
    {  
        // dd($id);
        $user = Notas::find($id);
            //dd( $user);
        if (is_null ($user)) {
            Flash::error('la consulta no se fue encontrado');

            return redirect(route('pregunta.index'));
        }

        $user->delete();

        Flash::success('La consulta fue eliminada exitosamente.');

        return redirect(route('pregunta.index'));
    }

    public function update( Request $request , $id )
    {  // dd($id);
         // dd($request->all());
                 
           $request->validate([
               'pregunta'=>'required',
               'descripcion'=>'required',
               'status'=>'required'
           ]);
           
           //dd($request->all());
          // Notas::create($request->all());

          //Notas::select('*')->where('id', '=', $id)->get();
          //dd($flight);
         $flight = Notas::find($id);
         $flight->pregunta = $request->pregunta;
         $flight->descripcion = $request->descripcion;
         $flight->status = $request->status;
         $flight->save();

        // $flight = $id == 0 ? new Notas() : Notas::find($id);
        // $flight->fill($request->all());
        // $flight->save();

        if (is_null($flight)) {
            Flash::error('error de consulta');

            return redirect(route('pregunta.index'));
        }

    
        Flash::success('La consulta fue modificada exitosamente.');

        return redirect(route('pregunta.index'));
    }

    
}

// |          GET|HEAD  | pregunta                                | pregunta.index                    | App\Http\Controllers\DashboardController@index                            | web,auth                                     |
// |        | POST      | pregunta                                | pregunta.store                    | App\Http\Controllers\DashboardController@store                            | web,auth                                     |
// |        | GET|HEAD  | pregunta/create                         | pregunta.create                   | App\Http\Controllers\DashboardController@create                           | web,auth                                     |
// |        | DELETE    | pregunta/{preguntum}                    | pregunta.destroy                  | App\Http\Controllers\DashboardController@destroy                          | web,auth                                     |
// |        | GET|HEAD  | pregunta/{preguntum}                    | pregunta.show                     | App\Http\Controllers\DashboardController@show                             | web,auth                                     |
// |        | PUT|PATCH | pregunta/{preguntum}                    | pregunta.update                   | App\Http\Controllers\DashboardController@update                           | web,auth                                     |
// |        | GET|HEAD  | pregunta/{preguntum}/edit               | pregunta.edit                     | App\Http\Controllers\DashboardController@edit                             | web,auth                                     |
// |     