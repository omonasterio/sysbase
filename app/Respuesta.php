<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Respuesta extends Model
{
    //
    protected $table = 'respuesta';
    
     protected $fillable = [
         'id_notas',
         'id_users',
         'pregunta',
         'respuesta',
         'fecha',
         'status'

        ];
}

//pregunta 
// status
// id_user
// 
//
//
