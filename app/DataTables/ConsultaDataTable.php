<?php

namespace App\DataTables;

use Illuminate\Support\Facades\Auth;
use App\Respuesta;
use App\Notas;

use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\EloquentDataTable;

class ConsultaDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        $dataTable = new EloquentDataTable($query);

        return $dataTable->addColumn('action', 'consulta.datatables_actions');
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\Post $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function queryz(Respuesta $model)
    {
        return $model->newQuery();
    }

    public function query(Notas $model) {
       
        return $model->newQuery()->select('*')->where('status', '=', 0);
    }


    public function querya(Respuesta $model) {
        $id =  Auth::user()->id;
        //dd($id);
        return $model->newQuery()->select('*')->where('id_user', '=', $id )->where('status', '=', $id );
    }

    //Notas
  //  $counto = DB::table('respuesta')->select('*')->where('id_users', '=', $id )->where('status', '=', $id )->get()->count(); 
    

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->addAction(['width' => '15%','printable' => false])
            ->parameters([
                'dom'     => 'Bfrtip',
                'order'   => [[0, 'desc']],
                'language' => ['url' => asset('js/SpanishDataTables.json')],
                //'scrollX' => false,
                'responsive' => true,
                'buttons' => [
                    ['extend' => 'create', 'text' => '<i class="fa fa-plus"></i> <span class="d-none d-sm-inline">Crear</span>'],
                    ['extend' => 'print', 'text' => '<i class="fa fa-print"></i> <span class="d-none d-sm-inline">Imprimir</span>'],
                    ['extend' => 'reload', 'text' => '<i class="fa fa-sync-alt"></i> <span class="d-none d-sm-inline">Recargar</span>'],
                    ['extend' => 'reset', 'text' => '<i class="fa fa-undo"></i> <span class="d-none d-sm-inline">Reiniciar</span>'],
                    ['extend' => 'export', 'text' => '<i class="fa fa-download"></i> <span class="d-none d-sm-inline">Exportar</span>'],
                ],
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {   // base de datos 
        return [
            'id', 
            'pregunta',
            'descripcion'
           
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'preguntaDataTable_' . time();
    }
}