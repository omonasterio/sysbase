<?php $__env->startSection('htmlheader_title'); ?>
    Log in
<?php $__env->stopSection(); ?>

<?php $__env->startPush('css'); ?>

<style>
    .background-image {
        position: absolute;
        left: 0;
        top: 0;
        background: url("<?php echo e(asset('/img/fondo_login.png')); ?>") no-repeat;
        background-size: cover;
        -moz-background-size: cover;
        -webkit-background-size: cover;
        -o-background-size: cover;
        width: 100%;
        height: 100%;
        -webkit-filter: blur(5px);
        -moz-filter: blur(5px);
        -o-filter: blur(5px);
        -ms-filter: blur(5px);
        filter: blur(5px);

    }
</style>
<?php $__env->stopPush(); ?>
<?php $__env->startSection('content'); ?>

    <body >
    <div class="background-image"></div>
    <div id="app" >

        <div class="login-box">

            <div class="card card-info card-outline">
                <div class="card-header">
                    <a href="<?php echo e(url('/home')); ?>"><b><?php echo e(config('app.name')); ?></b></a>
                </div>
                <div class="card-body">
                    <p class="text-info text-center"> <?php echo e(trans('adminlte_lang::message.siginsession')); ?> </p>

                    <login-form name="username"
                                domain="<?php echo e(config('auth.defaults.domain','')); ?>"></login-form>
                </div>
            </div>
            
            
            

        </div>


    </div>
    <?php echo $__env->make('layouts.partials.scripts', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <script>
        const app = new Vue({
            el: '#app'
        });
    </script>
    </body>

<?php $__env->stopSection(); ?>



<?php echo $__env->make('layouts.auth', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>