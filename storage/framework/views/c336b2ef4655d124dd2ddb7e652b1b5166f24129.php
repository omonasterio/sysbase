<?php $__env->startPush('css'); ?>
<link rel="stylesheet" href="<?php echo e(asset('plugins/bootstrap-fileinput/css/fileinput.min.css')); ?>" />
<?php $__env->stopPush(); ?>
<?php $__env->startPush('scripts'); ?>
<script type="text/javascript" src="<?php echo e(asset('plugins/bootstrap-fileinput/js/fileinput.js')); ?>"></script>
<script type="text/javascript" src="<?php echo e(asset('plugins/bootstrap-fileinput/js/locales/es.js')); ?>"></script>
<?php $__env->stopPush(); ?>