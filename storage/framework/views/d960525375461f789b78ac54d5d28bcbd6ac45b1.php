<?php $__env->startSection('css'); ?>
<?php echo $__env->make('layouts.datatables_css', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php $__env->stopSection(); ?>



<?php $__env->startSection('htmlheader_title'); ?>
Constas reponder
<?php $__env->stopSection(); ?>


<?php $__env->startSection('content'); ?>
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row">
                <div class="col">
                    <h1 class="m-0 text-dark">
                        Responder la Conslata
                    </h1>
                </div><!-- /.col -->
                <div class="col ">
                    
                    
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
        <div class="container-fluid">
            <?php echo $__env->make('flash::message', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?> 
            <?php echo $__env->make('adminlte-templates::common.errors', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>

            <div class="row">
                <?php $__currentLoopData = $pregunta; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $pre): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-body">
                       
                       
                            <div class="form-group col-sm-12" id="<?php echo e($pre['id']); ?>">
                                <label for="username" class=""> <?php echo e($pre['pregunta']); ?> </label>
                                <br>
                                <span>
                                      <?php echo e($pre['descripcion']); ?> 
                                </span>
                                <hr>
                         

                            <form method="POST" action="/consulta/respuestas/<?php echo e($pre['id']); ?>">
                                <input name="_token" type="hidden" value="<?php echo e(csrf_token()); ?>"/>
                                <input type="hidden" value="<?php echo e($pre['id']); ?>" name="id">
                                    <input class="form-control"  value="<?php echo e($pre['pregunta']); ?>" name="pregunta" type="hidden" id="pregunta">
                                    <input class="form-control" name="respuesta" type="text" id="respuesta">
                                    
                                
                                </div>
                                    
                                        <!-- Submit Field consultas.store -->
                                        <div class="form-group col-sm-12">
                                           
                                            <button type="submit" class="btn btn-outline-success">Guardar</button>
                                           
                                        </div>
                                    </form>
                                    
                                </div>
                    </div>
                    </div>
                    <!-- /.card -->
                </div>
                <!-- /.col-md-6 -->
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </div>
    <!-- /.content -->


<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>