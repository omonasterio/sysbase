<?php echo $__env->make('layouts.plugins.select2', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php echo $__env->make('layouts.plugins.bootstrap_fileinput', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>

<?php $__env->startSection('htmlheader_title'); ?>
	Editar perfil
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Editar perfil</h1>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
        <div class="container-fluid">
            <?php echo $__env->make('flash::message', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
            <?php echo $__env->make('adminlte-templates::common.errors', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>

            <?php echo Form::model($user, ['route' => ['user.update.profile', $user->id], 'method' => 'patch','enctype' => "multipart/form-data"]); ?>

            <div class="row">
                <div class="col-lg-5">
                    <div class="card" >
                        <img class="card-img-top" src="<?php echo e(Auth::user()->imagen()); ?>" alt="Card image cap" id="img-user">
                        <div class="card-body" style="padding: 0px">
                            <!-- Imagen Field -->

                            <div class="form-row " id="field-img" style="display: none">
                                <div class="form-group col-sm-12">
                                <input id="files" name="imagen" type="file">
                                </div>
                            </div>
                            <a class="btn btn-outline-info btn-sm btn-block" id="etidarImagen">Editar</a>
                        </div>
                    </div>
                    <!-- /.card -->
                </div>

                <div class="col-lg-7">
                    <div class="card">
                        <div class="card-body">

                            <div class="form-row">
                                <!-- Name Field -->
                                <div class="form-group col-sm-6">
                                    <?php echo Form::label('name', 'Name:'); ?>

                                    <?php echo Form::text('name', null, ['class' => 'form-control']); ?>

                                </div>

                                <!-- Email Field -->
                                <div class="form-group col-sm-6">
                                    <?php echo Form::label('email', 'Email:'); ?>

                                    <?php echo Form::email('email', null, ['class' => 'form-control']); ?>

                                </div>
                            </div>
                        </div>
                    </div>


                    <div class="card card-default collapsed-card">
                        <div class="card-header">
                            <h3 class="card-title">Editar contraseña</h3>

                            <div class="card-tools">
                                <button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-edit"></i>
                                </button>
                            </div>
                            <!-- /.card-tools -->
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <div class="form-row">
                            <!-- Password Field -->
                            <div class="form-group col-sm-6">
                                <?php echo Form::label('password', 'password:'); ?>

                                <?php echo Form::password('password', ['class' => 'form-control']); ?>

                            </div>

                            <!-- Password Field -->
                            <div class="form-group col-sm-6">
                                <?php echo Form::label('password_confirmation', 'Confirmar password:'); ?>

                                <?php echo Form::password('password_confirmation', ['class' => 'form-control']); ?>

                            </div>
                            </div>
                        </div>
                        <!-- /.card-body -->
                    </div>
                </div>
                <!-- /.col-md-6 -->
            </div>
            <!-- /.row -->

            <div class="row">
                <!-- Submit Field -->
                <div class="form-group col-sm-12">
                    <button type="submit" onClick="this.form.submit(); this.disabled=true;" class="btn btn-outline-success">Guardar</button>
                    <a href="<?php echo e(URL::previous()); ?>" class="btn btn-default">Cancelar</a>
                </div>
            </div>
            <?php echo Form::close(); ?>

        </div>
        <!-- /.container-fluid -->
    </div>
    <!-- /.content -->
<?php $__env->stopSection(); ?>
<?php $__env->startPush('scripts'); ?>
<script>
    $(function () {
        $("#etidarImagen").click(function () {
           console.log('editar imagen');
            $('#field-img').show();
            $('#img-user').hide();

        });

        var $input = $("#files");
        $input.fileinput({
            
            //            uploadAsync: false,
            showUpload: false, // hide upload button
            showRemove: false, // hide remove button
//            minFileCount: 1,
//            maxFileCount: 5,
            allowedFileExtensions: ["png","bmp","gif","jpg","pdf"],
        });
    })
</script>
<?php $__env->stopPush(); ?>
<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>