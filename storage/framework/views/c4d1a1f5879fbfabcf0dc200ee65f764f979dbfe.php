
<!-- Descripcion Field -->
<div class="form-group col-sm-6">
    <?php echo Form::label('name', 'Nombre:'); ?>

    <?php echo Form::text('name', null, ['class' => 'form-control']); ?>

    <?php echo Form::hidden('guard_name', 'web'); ?>

</div>


<div class="form-group col-sm-12">
    <?php echo Form::label('permisos_id','Permisos: '); ?>

    <a class="success" data-toggle="modal" href="#modal-form-permissions" tabindex="1000">nuevo</a>
    <div class="row">
        <div class="col-5">
            <select name="permission_from[]" id="multiselect" class="form-control multiselect-two-sides" size="8" multiple="multiple">
                <?php $__currentLoopData = App\Models\Permission::all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $permission): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <option value="<?php echo e($permission->name); ?>"><?php echo e($permission->name); ?></option>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </select>
        </div>
        <div class="col-2">
            <button type="button" id="multiselect_rightAll" class="btn btn-block btn-outline-default btn-multiselect"><i class="fa fa-forward"></i></button>
            <button type="button" id="multiselect_rightSelected" class="btn btn-block btn-outline-default btn-multiselect"><i class="fa fa-chevron-right"></i></button>
            <button type="button" id="multiselect_leftSelected" class="btn btn-block btn-outline-default btn-multiselect"><i class="fa fa-chevron-left"></i></button>
            <button type="button" id="multiselect_leftAll" class="btn btn-block btn-outline-default btn-multiselect"><i class="fa fa-backward"></i></button>
        </div>
        <div class="col-5">
            <select name="permission_to[]" id="multiselect_to" class="form-control" size="8" multiple="multiple">
                <?php if(isset($rol)): ?>
                <?php $__currentLoopData = $rol->permissions; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $permission): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <option value="<?php echo e($permission->name); ?>"><?php echo e($permission->name); ?></option>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                <?php endif; ?>
            </select>
        </div>
    </div>
</div>