<?php $__env->startPush('css'); ?>
<link rel="stylesheet" href="<?php echo e(asset('plugins/jquery-ui-dist/jquery-ui.min.css')); ?>" />
<?php $__env->stopPush(); ?>
<?php $__env->startPush('scripts'); ?>
<script type="text/javascript" src="<?php echo e(asset('plugins/jquery-ui-dist/jquery-ui.min.js')); ?>"></script>
<?php $__env->stopPush(); ?>