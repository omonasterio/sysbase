<?php $__env->startPush('css'); ?>
<link rel="stylesheet" href="<?php echo e(asset('plugins/select2/dist/css/select2.min.css')); ?>" />
<?php $__env->stopPush(); ?>
<?php $__env->startPush('scripts'); ?>
<script type="text/javascript" src="<?php echo e(asset('plugins/select2/dist/js/select2.full.js')); ?>"></script>
<script type="text/javascript" src="<?php echo e(asset('plugins/select2/dist/js/i18n/es.js')); ?>"></script>
<?php $__env->stopPush(); ?>