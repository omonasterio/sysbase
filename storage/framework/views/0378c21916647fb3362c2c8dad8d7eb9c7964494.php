
<a href="<?php echo e(route('users.show', $id)); ?>" class='btn btn-default btn-xs' data-toggle="tooltip" title="Ver">
    <i class="fa fa-eye"></i>
</a>

<a href="<?php echo e(route('users.edit', $id)); ?>" class='btn btn-info btn-xs' data-toggle="tooltip" title="Editar">
    <i class="fa fa-edit"></i>
</a>

<a href="<?php echo e(route('user.menu',$id)); ?>" class="btn btn-sm btn-default">
    <span class="fa fa-list-alt" data-toggle="tooltip" title="Menu"></span>
</a>

<a href="#" onclick="deleteThis(this)" data-id="<?php echo e($id); ?>" data-toggle="tooltip" title="Eliminar" class='btn btn-danger btn-xs'>
    <i class="fa fa-trash-alt"></i>
</a>
<form action="<?php echo e(route('users.destroy', $id)); ?>" method="POST" id="delete-form<?php echo e($id); ?>">
    <?php echo method_field('DELETE'); ?>
    <?php echo csrf_field(); ?>
</form>