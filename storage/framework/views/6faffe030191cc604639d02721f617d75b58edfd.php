<?php $__env->startSection('css'); ?>
    <?php echo $__env->make('layouts.datatables_css', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php $__env->stopSection(); ?>


<?php $__env->startSection('htmlheader_title'); ?>
    Pregunta 
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.plugins.jquery-ui', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>


<?php $__env->startSection('content'); ?>
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col">
                    <h1 class="m-0 text-dark">Dashboard</h1>
                </div>
                <!-- 
                    head de la pag
                 -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">        
                <?php echo $__env->make('flash::message', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                <?php echo $__env->make('adminlte-templates::common.errors', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
            <div class="row">
                <!-- formulario de la consulta  -->

                <div class="card">
                    <div class="card-body">
                            
                            
                                <div class="form-row">

                                    <!-- ('titulo');
                                    ('descripcion'); -->

                                <!-- Username Field -->
                                <div class="form-group col-sm-10">
                                    <label for="pregunta">Pregunta:</label>
                                    <br>
                                        <span> <?php echo e($pregunta); ?> </span>
                                    
                                </div>
                                <?php echo Form::open(['route' => 'consultas.store']); ?>

                               
                                        <?php echo e(csrf_field()); ?>

                           
                                <!-- Name Field -->
                                <div class="form-group col-sm-10">
                                    <label for="descripcion"> Su Respuestas :</label>
                                    <input class="form-control" name="descripcion" placeholder="descripcion de la consulata" type="text" id="descripcion">
                                </div>

                                                          

                                    <!-- Submit Field -->
                                    <div class="form-group col-sm-12">

                                        <button type="submit" onclick="this.form.submit(); this.disabled=true;"
                                            class="btn btn-outline-success waves-effect waves-light">Guardar</button>
                                        <a href="<?php echo e(route('dashboard')); ?>"
                                        class="btn btn-outline-default waves-effect waves-light">Cancelar</a>
                                    </div>
                                    <?php echo Form::close(); ?>

                        </div>
                    </div>
                </div>
                
                <!-- formulario de la consulta  -->
        
            </div>

           

        </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
<?php $__env->stopSection(); ?>



<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>