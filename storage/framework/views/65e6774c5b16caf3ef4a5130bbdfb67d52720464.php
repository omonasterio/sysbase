<!-- Username Field -->
<div class="form-group col-sm-3">
    <?php echo Form::label('username', 'Username:'); ?>

    <?php echo Form::text('username', null, ['class' => 'form-control']); ?>

</div>

<!-- Name Field -->
<div class="form-group col-sm-3">
    <?php echo Form::label('name', 'Name:'); ?>

    <?php echo Form::text('name', null, ['class' => 'form-control']); ?>

</div>

<!-- Email Field --> 
<div class="form-group col-sm-6">
    <?php echo Form::label('email', 'Email:'); ?>

    <?php echo Form::email('email', null, ['class' => 'form-control']); ?>

</div>

<!-- Rols Field -->
<div class="form-group col-sm-6">
    <?php echo Form::label('rols', 'Roles:'); ?>


    <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('Crear Rol')): ?>
        <a class="success" data-toggle="modal" href="#modal-form-roles" tabindex="1000">nuevo</a>
    <?php endif; ?>
    <?php echo Form::select(
            'rols[]',
            slc(\App\Models\Role::class,'name','name',null),
            isset($userEdit) ? $userEdit->getRoleNames() : null,
            ['id'=>'rols','class' => 'form-control', 'multiple' => 'multiple','style'=>'width: 100%']
        ); ?>

</div>

<!-- Permisos Field -->
<div class="form-group col-sm-6">
    <?php echo Form::label('permisos', 'Permisos Directos:'); ?>


    <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('crear permiso')): ?>
        <a class="success" data-toggle="modal" href="#modal-form-permissions" tabindex="1000">nuevo</a>
    <?php endif; ?>

    <?php echo Form::select(
            'permisos[]',
            slc(\App\Models\Permission::class,'name','name',null),
            isset($userEdit) ? $userEdit->getDirectPermissions()->pluck('name','name') : null,
            ['id'=>'permisos','class' => 'form-control', 'multiple' => 'multiple','style'=>'width: 100%']
        ); ?>

</div>

<div class="form-group col-sm-12" style="padding: 0px; margin: 0px">
</div>
<?php if(!isset($create)): ?>
    <div class="form-group col-sm-12">
        <a class="" role="button" data-toggle="collapse" href="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
            <span class=" btn btn-outline-primary">Editar también contraseña</span>
        </a>
    </div>
<?php endif; ?>


<div class="col-sm-12 <?php echo e(!isset($create) ? "collapse" : ''); ?>" id="collapseExample">
    <div class="form-row">

    <!-- Password Field -->
    <div class="form-group col-sm-6">
        <?php echo Form::label('password', 'password:'); ?>

        <?php echo Form::password('password', ['class' => 'form-control']); ?>

    </div>

    <!-- Password Field -->
    <div class="form-group col-sm-6">
        <?php echo Form::label('password_confirmation', 'Confirmar password:'); ?>

        <?php echo Form::password('password_confirmation', ['class' => 'form-control']); ?>

    </div>
    </div>
</div>

<?php if(!isset($create)): ?>
<!-- Imagen Field -->
<div class="form-group col-sm-4">
    <div class="card" >
        <img class="card-img-top" src="<?php echo e($userEdit->imagen()); ?>" alt="Card image cap" id="img-user">
        <div class="card-body" style="padding: 0px">
            <!-- Imagen Field -->

            <div class="form-row " id="field-img" style="display: none">
                <div class="form-group col-sm-12">
                    <input id="files" name="imagen" type="file">
                </div>
            </div>
            <a  href="#" class="btn btn-outline-info btn-sm btn-block" id="etidarImagen">Editar</a>
        </div>
    </div>
</div>
<?php else: ?>
    <!-- Imagen Field -->
    <div class="form-group col-sm-4">
        <input id="files" name="imagen" type="file">
    </div>
<?php endif; ?>

<?php $__env->startPush('scripts'); ?>
<script>
    $(function () {
        $("#etidarImagen").click(function (e) {
            e.preventDefault();
            console.log('editar imagen');
            $('#field-img').show();
            $('#img-user').hide();

        });
        $("#rols,#permisos").select2();

        var $input = $("#files");
        $input.fileinput({
            
//            uploadAsync: false,
            showUpload: false, // hide upload button
            showRemove: false, // hide remove button
//            minFileCount: 1,
//            maxFileCount: 5,
            allowedFileExtensions: ["png","bmp","gif","jpg","pdf"],
        });
    })
</script>
<?php $__env->stopPush(); ?>